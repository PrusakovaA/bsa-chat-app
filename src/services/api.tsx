import DataService from "./DataService";

export class Api {
  url: string = '';
  constructor(url: string) {
    this.url = url
}

    async getMessages() {
        const url: string = this.url;
        try {
            let res = await fetch(url);
          return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    async loadData() {
    const messages = await this.getMessages();
    messages.map((messages: { time: string; createdAt: string | Date; }) => (
      messages.time = DataService.getTime(messages.createdAt)
      ));
    const usersCount = DataService.getUsersCount(messages);
    return { messages, usersCount };
    }

   
}



