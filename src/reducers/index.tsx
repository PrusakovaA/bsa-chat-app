import { combineReducers } from "redux";
import chat from "../components/Chat/reducer"
import message from "../components/Message/reducer"

const rootReducer = combineReducers({
    chat,
    message
});

export default rootReducer;