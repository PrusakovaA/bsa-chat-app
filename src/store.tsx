import { createStore, compose } from "redux";
import rootReducer from "./reducers";


export default createStore(rootReducer);