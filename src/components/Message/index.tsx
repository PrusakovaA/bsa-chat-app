import React from "react";
import { connect } from "react-redux";
import Message from "../../interfaces/IMessage";
import { changeLike } from "../Chat/actions";
import OwnMessage from "./OwnMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import "./index.css";
interface IMessageProps {
    message: Message;
    changeLike: Function;
}

interface MessageState {
    likesMessage: boolean;
}

class MessageClass extends React.Component<IMessageProps, MessageState> {
      constructor(props: IMessageProps) {
        super(props);
        this.state = {
            likesMessage: false,
        };
      }
      onLike() {
        this.props.changeLike(this.props.message.id);
        this.setState({likesMessage: !this.state.likesMessage});
      }

      getUsersMessages(message: Message, id: string) {
        if (message.user === "Me") {
          return (
            <OwnMessage
              key={id}
              message={message}
            />
          );
        } else {
          return (<div className="others-message">
          <div className="message-content">
          <div className="message-user-avatar">
              <img
                className="avatar"
                src={this.props.message.avatar}
                alt="avatar"
              />
            </div>
            <div className="text">
          <div className="message-text">{this.props.message.text}</div>
          </div>
            <div className="message-info">
            <span className="message-user-name">{this.props.message.user}</span>
              <span className="message-time">{this.props.message.time}</span>
              <div className="like" onClick={() => this.onLike()}>
                <span>{this.state.likesMessage ? <FontAwesomeIcon icon={ faHeart} className="message-liked"/> 
                : <FontAwesomeIcon icon={ faHeart} className="message-like"/>}</span>
            </div>
            </div>
        </div>  
        </div>
          );
        }
      }

      render() {
        return( <div className="message">
         {this.getUsersMessages(this.props.message, this.props.message.id)}
        </div>
        );
      }
}

const mapDispatchToProps = {
  changeLike
};

export default connect(null, mapDispatchToProps)(MessageClass);