import { OwnMessageAction } from "./actionTypes";

interface MessageState {
    editModal: boolean;
    id: string;
  }


const initialState: MessageState = {
  editModal: false,
    id: ""
  };

  interface MessageAction {
    type: OwnMessageAction;
    payload?: {
      id?: string;
    };
  }

export default function (state = initialState, action: MessageAction) {
    switch (action.type) {
        case OwnMessageAction.SET_MESSAGE_ID: {
            const { id } = action.payload!;
            return { 
              ...state, 
              id: id 
            };
          }
        case OwnMessageAction.SHOW_MODAL: {
            return { 
              ...state, 
              editModal: true 
            };
          }
        case OwnMessageAction.HIDE_MODAL: {
            return { 
              ...state, 
              editModal: false 
            };
          }
          default:
      return state;
  }
}
          
          