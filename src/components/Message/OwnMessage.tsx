import React from "react";
import { connect } from "react-redux";
import { deleteMessage, editMessage } from "../Chat/actions";
import {showModal, setMessageId,} from "./actions";
import Message from "../../interfaces/IMessage";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import "./index.css";

interface IOwnMessageProps {
  message: Message;
  editMessage: Function;
  deleteMessage: Function;
  showModal: Function;
  setMessageId: Function;
}

interface OwnMessageState{
}

class OwnMessage extends React.Component<IOwnMessageProps, OwnMessageState>{

  onDelete() {
    this.props.deleteMessage(this.props.message.id);
  }
      onEdit() {
        this.props.setMessageId(this.props.message.id);
        this.props.showModal();
      }


      render() {
        return (
        <div className="own-message">
                <div className="message-text">
                    {this.props.message.text}
                    </div>
            <div className="message-info">
                <span className="me-user"></span>
                <span className="message-time">{this.props.message.time}</span>
                <div
                        className="message-edit"
                        onClick={() => this.onEdit()}
                >
                         <FontAwesomeIcon icon={faCog} />
                    </div>
                    <div
                        className="message-delete"
                        onClick={() => this.onDelete()}
                    > 
                         <FontAwesomeIcon icon={faTrashAlt} />
                    </div>
                </div>
        </div>
        );
      }
}   

const mapDispatchToProps = {
  deleteMessage,
  editMessage,
  showModal,
  setMessageId,
};

export default connect(null,mapDispatchToProps)(OwnMessage);