import { OwnMessageAction } from "./actionTypes";

export const setMessageId = (id:string) => ({
    type: OwnMessageAction.SET_MESSAGE_ID,
    payload: {
        id
    }
});

export const setEdited = () => ({
    type: OwnMessageAction.SET_EDITED
  });

  export const showModal = () => ({
    type: OwnMessageAction.SHOW_MODAL
});

export const hideModal = () => ({
    type: OwnMessageAction.HIDE_MODAL
});