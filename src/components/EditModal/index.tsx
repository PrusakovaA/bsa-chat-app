import React from "react";
import { connect } from "react-redux";
import { editMessage } from "../Chat/actions";
import { hideModal } from "../Message/actions";
import Message from "../../interfaces/IMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import "./index.css";

interface EditModalProps {
  messages?: Message[];
  id?: string;
  editMessage: Function;
  hideModal: Function;
}

interface ModalState {
  text: string;
}

class EditModal extends React.Component<EditModalProps, ModalState> {
  constructor(props: EditModalProps) {
    super(props);
    this.state = {
      text: "",
    };
    this.onEdit = this.onEdit.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onTyping = this.onTyping.bind(this);
  }

  componentDidMount() {
    const messageId = this.props.id;
    const currMessage = this.props.messages!.find(
      (message) => message.id === messageId
    );
    this.setState({ text: currMessage!.text });
  }

  onTyping(event: React.FormEvent<HTMLInputElement>) {
    this.setState({ text: event.currentTarget.value });
  }

  onEdit() {
    this.props.editMessage(this.props.id, {
      text: this.state.text,
    });
    this.props.hideModal();
  }

  onClose() {
    this.props.hideModal();
  }


  render() {
    let text = this.state.text;
    return (
      <div className="edit-message-modal">
        <div className="modal-shown">
          <div className="modal-header">
            <span>Edit Message</span>
          </div>
          <div className="modal-body">
            <div className="edit-message-input">
              <input
                type="text"
                className="edit-text-area"
                value={text}
                onChange={this.onTyping}
              />
              <div className="buttons">
              <button className="edit-message-button" onClick={this.onEdit}>
              <FontAwesomeIcon icon={faCheck} />
              </button>
              <button
                className="edit-message-close"
                onClick={() => this.onClose()}
              >
                <FontAwesomeIcon icon={faTimes} />
              </button>
              </div>
            </div>
          </div>
        </div>
        </div>
    );
  }
}

interface Store{
  chat: {
    messages?: Message[];
  };
  message: {
    id: string;
  };
}


const mapStateToProps = (state: Store) => {
  return {
    messages: state.chat.messages,
    id: state.message.id,
  };
};

const mapDispatchToProps = {
  editMessage,
  hideModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);