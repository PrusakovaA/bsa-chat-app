import React from "react";
import { connect } from "react-redux";
import { addMessage } from "../Chat/actions";
import {setMessageId, showModal} from "../Message/actions";
import data from "../../services/DataService";
import Message from "../../interfaces/IMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import './index.css';

interface IMessageInputProps {
  messages: Message[];
  addMessage: Function;
  setMessageId: Function;
  showModal: Function;
}

interface MessageInputState {
  messageBody: string;
  }

class MessageInput extends React.Component<IMessageInputProps, MessageInputState>{
    constructor(props: IMessageInputProps) {
        super(props);
        this.state = {
          messageBody: "",
        };
        this.onTyping = this.onTyping.bind(this);
      }

      onSend() {
        const date: Date = new Date();
        const message: Message = {
          id: date.getTime().toString(),
          user: "Me",
          userId: "1",
          text: this.state.messageBody,
          createdAt: new Date(),
          time: data.getTime(date),
          likesCount: 0
        };
        this.props.addMessage(message);
      }

      onTyping(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ messageBody: event.currentTarget.value });
      }
     
    

      render() {
        return (
          <div className="message-input">
            <form className="message-input-text">
              <input
                type="text"
                className="text-area"
                placeholder="Message"
                value={this.state.messageBody}
                onChange={this.onTyping}
              />
            </form>
            <button className="message-input-button"
              onClick={() => this.onSend()}
            >
              <FontAwesomeIcon icon={faPaperPlane} className="send"/>
            </button>
          </div>
        );
      }
    }

    interface Store {
      chat: {
        messages?: Message[];
      };
    }
    
    const mapStateToProps = (state: Store) => {
      return {
        messages: state.chat.messages!,
      };
    };
    
    const mapDispatchToProps = {
      addMessage,
      showModal,
      setMessageId
    };
    
    export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);