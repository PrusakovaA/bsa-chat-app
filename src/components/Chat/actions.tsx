import { ChatAction } from "./actionTypes";
import Message from "../../interfaces/IMessage";

export const addMessage = (data: Message) => ({
    type: ChatAction.ADD_MESSAGE,
    payload: {
        data
    }
});

export const editMessage = (id: string, data: Message) => ({
    type: ChatAction.EDIT_MESSAGE,
    payload: {
        id,
        data
    }
});

export const deleteMessage = (id: Message)=> ({
    type: ChatAction.DELETE_MESSAGE,
    payload: {
        id
    }
});


export const changeLike = (id: string) => ({
    type: ChatAction.CHANGE_LIKE,
    payload: {
      id
    },
  });

  export const hideLoading = () => ({
    type: ChatAction.HIDE_LOADING,
  });

  export const init = (messages: Message[], usersCount:number) => ({
    type: ChatAction.INIT,
    payload: {
      messages,
      usersCount,
    },
  });
  