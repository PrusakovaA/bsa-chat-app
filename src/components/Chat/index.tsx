import React from "react";
import { connect } from "react-redux";
import Header from "../Header/index";
import MessageInput from "../MessageInput/index";
import Preloader from "../Preloader/index";
import MessageList from "../MessageList/index";
import Message from "../../interfaces/IMessage";
import { Api } from "../../services/api";
import AppHeader from "../AppHeader";
import * as chatAction from "./actions";
import "./index.css";


interface IChatProps{
  preloader: boolean;
  messages?: Message[];
  usersCount?: number;
  hideLoading: Function;
  addMessage: Function;
  init: Function;
}
interface ChatState { 
}

class Chat extends React.Component<IChatProps,ChatState> {
  api: Api;
  url="https://edikdolynskyi.github.io/react_sources/messages.json"
    constructor(props: IChatProps) {
        super(props);
        this.api = new Api(this.url);  
    }

    componentDidMount() {
      this.api.loadData().then(({ messages, usersCount }) => {
        this.props.init(messages, usersCount);
        this.props.hideLoading();
      });
      }
 

    render() {
        const data = this.props
        return (<div className="chat">
        <AppHeader />
          {data.preloader
                ? (<Preloader />) : (
                  <div className="chat-container">
                <Header
                title={"My chat"}
                usersCount={data.usersCount!}
                messagesCount={data.messages!.length}
                lastMessageDate={this.props.messages![this.props.messages!.length - 1].time!}
                />
                <div className="chat-wrapper">
                <MessageList
               messages={this.props.messages!}
              />
                </div>
                <MessageInput />
            </div>
                )
          }
           </div>
           
        );
    }
}
interface Store {
  chat: {
    preloader: boolean;
    messages?: Message[];
    usersCount?: number;
  };
}

const mapStateToProps = (state: Store) => {
  return {
    preloader: state.chat.preloader,
    messages: state.chat.messages,
    usersCount: state.chat.usersCount,
  };
};

const mapDispatchToProps = {
  init: chatAction.init,
  hideLoading: chatAction.hideLoading,
  addMessage: chatAction.addMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);   
