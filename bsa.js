import Chat from "./src/components/Chat/index"
import rootReducer from "./reducers";

export default {
    Chat,
    rootReducer,
};